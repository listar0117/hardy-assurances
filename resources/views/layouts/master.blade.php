
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}" defer></script>

  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
  <div class="wrapper">
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a href="{{ url('/logout') }}" class="nav-link" id="logout"
              onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                Log out
            </a>

             <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                 {{ csrf_field() }}
                 <input type="submit" value="logout" style="display: none;">
             </form>
          </li>
      </ul>
    </nav>

    <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <a href="{{ route('dashboard') }}" class="brand-link">
        <i class="fas fa-hospital-symbol brand-image"></i>
        <span class="brand-text font-weight-light">Hardy Assurances</span>
      </a>

      <div class="sidebar">
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
              <a href="{{ route('customers.index') }}" class="nav-link {{ (Request::segment(1)=='customers') ? 'active' : '' }}">
                <i class="nav-icon fas fa-users"></i>
                <p>Customers</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('contracts.index') }}" class="nav-link {{ (Request::segment(1)=='contracts') ? 'active' : '' }}">
                <i class="nav-icon fas fa-file-signature"></i>
                <p>Contacts</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('products.index') }}" class="nav-link {{ (Request::segment(1)=='products') ? 'active' : '' }}">
                <i class="nav-icon fas fa-archive"></i>
                <p>Products</p>
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </aside>

    <div class="content-wrapper">
      <div class="content">
        <div id="app" class="container-fluid">
          @yield('content')
        </div>
      </div>
    </div>
  </div>
</body>
</html>
