@extends('layouts.master')

@section('content')
    <dashboard :customer-list="{{ $customers }}"></dashboard>
@endsection
