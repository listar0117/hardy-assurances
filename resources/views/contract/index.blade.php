@extends('layouts.master')

@section('content')
    <contract-all-list :contract-list="{{ $contract_list }}"></contract-all-list>
@endsection
