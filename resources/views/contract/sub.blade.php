@extends('layouts.master')

@section('content')
    <contract-list :customer="{{ $customer }}" :contract-list="{{ $contract_list }}"></contract-list>
@endsection
