<html>
	<style>
		table, th, td {
			border: 1px solid #ddd;
			border-collapse: collapse;
		}

	</style>
	<body style="position:relative;text-align: center; margin: 0 auto; width: 700px; padding: 30px 0;">	

			<div style="text-align: left; display: inline-block; vertical-align: middle; width: 49%; ">
				<div style="margin-bottom: 10px;">
					<div style="padding-left: 10px;">
						<p style="margin: 0; font-size: 17px; font-weight: 500;"><strong>EIRL Jerome HARDY</strong></p>
						<p style="margin: 0;">Courtier en Assurances</p>
						<p style="margin: 0;">43 rue de mulhouse</p>
						<p style="margin: 0;">21000 DIJON</p>
					</div>
					<div style="padding-left: 10px; margin: 15px 0;">
						<p style="margin: 0; font-size: 17px; font-weight: 500;">Tel : 03 80 74 40 40</p>
						<a href="#" style="margin: 0; text-decoration: none;">f.oropallo.assurancebateau@gmail.com</a>
					</div>
					<div style="padding-left: 10px;">
						<p style="margin: 0; font-size: 17px; font-weight: 500;">ORIAS : 07 021 457</p>
						<p style="margin: 0;">ACPR : 4 place de Budapest</p>
						<p style="margin: 0;">75436 PARIS Cedex 09</p>
						<p style="margin: 0;">Site : www.orias.fr</p>
					</div>			
				</div>
			</div>	

			<div style="display: inline-block; vertical-align:top; width: 283px; text-align: left;">
				<img src="http://www.hardy.assurances-bateau.net/images/logo.png" width="283" height="125" alt="logo"/>
				<div style="margin: 50px 0;">
					<div style="padding-left: 10px;">
						<p style="margin: 0; font-size: 17px; font-weight: 500;"><strong>{{ $contract->customer->name }}</strong></p>
						<p style="margin: 0;">{{ $contract->customer->address1 }}</p>
						<p style="margin: 0;">{{ $contract->customer->address2 }}</p>
						<p style="margin: 0;">{{ $contract->customer->address3 }}</p>
						<p style="margin: 0;">{{ $contract->customer->country }}</p>
						<br>
						<p style="margin: 0;">Date : {{ \Carbon\Carbon::parse($contract->updated_at)->format('d/m/Y')}}</p>
					</div>
		        </div>
		    </div>

			<div style="margin: 30px 0; font-size: 30px; font-weight: 500;">Appel de cotisations</div>
			<table class="table" style="margin: 0 auto 30px; text-align: center; padding: 5px; width: 100%;">
			  <thead style="background: lightgray;">
			    <tr>
			      <td style="padding:5px 15px; border: 1px sol">No</td>
			      <td style="padding:5px 15px; ">Description</td>
			      <td style="padding:5px 15px;">Montant HT</td>
			      <td style="padding:5px 15px; ">Taxes</td>
			      <td style="padding:5px 15px; ">Total TTC</td>
			    </tr>
			  </thead>
			  <tbody>
                @foreach ($contract->products as $product)
                    <tr>
                        <td style="padding: 5px 15px; ">{{ $loop->iteration }}</td>
                        <td style="padding: 5px 15px; text-align: left; ">{{ $product->name }}</td>
                        <td style="padding: 5px 15px; "> {{ number_format($product->real_price, 2) }} {{ config('emmtek.config.currency') }}</td>
                        <td style="padding: 5px 15px; ">{{ number_format($product->calc_taxes, 2) }} {{ config('emmtek.config.currency') }}</td>
                        <td style="padding: 5px 15px; ">{{ number_format($product->real_price + $product->calc_taxes, 2) }} {{ config('emmtek.config.currency') }}</td>
                    </tr>
                @endforeach
			    <tr>
			      <td style="padding: 5px 15px; " colspan="2">Total</td>
			      <td style="padding: 5px 15px; ">{{ number_format($contract->sum_price, 2) }} {{ config('emmtek.config.currency') }}</td>
			      <td style="padding: 5px 15px; ">{{ number_format($contract->sum_taxes, 2) }} {{ config('emmtek.config.currency') }}</td>
			      <td style="padding: 5px 15px; ">{{ number_format($contract->sum_price + $contract->sum_taxes, 2) }} {{ config('emmtek.config.currency') }}</td>
			    </tr>
			  </tbody>
			</table>

			<table class="table" style="margin: 0 auto; text-align: center; border: 1px solid #ddd; padding: 5px; float: right;">
			  <tbody>
			    <tr>
			      <td style="padding: 5px 15px; border: 1px solid #ddd;">R&eacute;f&eacute;rence contrat</td>
			      <td style="padding: 5px 15px; border: 1px solid #ddd;">{{ $contract->contract_number }}</td>
			    </tr>
			    <tr>
			      <td style="padding: 5px 15px; border: 1px solid #ddd;">P&eacute;riode</td>
			      <td style="padding: 5px 15px; border: 1px solid #ddd;">
{{ \Carbon\Carbon::parse($contract->start_date)->format('d/m/Y')}} - {{ \Carbon\Carbon::parse($contract->end_date)->format('d/m/Y')}}</td>
			    </tr>
			  </tbody>
			</table>
			<div style="position: absolute; bottom: -50px; left: 0; right:0; margin: 0 auto; text-align: center;">RCS 432398777– Garanties financières et Responsabilité Civile Professionnelle CGPA</div>
	</body>
</html>
