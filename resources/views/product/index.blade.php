@extends('layouts.master')

@section('content')
    <product-list :product-list="{{ $product_list }}"></product-list>
@endsection
