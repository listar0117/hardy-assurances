@extends('layouts.master')

@section('content')
    <customer-list :customer-list="{{ $customer_list }}"></customer-list>
@endsection
