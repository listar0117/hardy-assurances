<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


Route::get('/', 'HomeController@index')->name('dashboard');

Route::resource('/customers', 'CustomerController');
Route::resource('/contracts', 'ContractController');
Route::resource('/products', 'ProductController');

Route::get('/contract/{id}', 'HomeController@showContracts');
Route::get('/product/all-product', 'ProductController@getAllProduct');
Route::get('/get-countries','HomeController@getCountries');
Route::post('/contracts/copy/{id}','ContractController@copyContract');
Route::get('/generate-pdf/{id}','HomeController@generatePDF');