<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $product_list = Product::all();
        return view('product.index', compact('product_list'));
    }

    public function create()
    {
        // return view('person.create');
    }


    public function store(Request $request)
    {
        
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'price' => ['required', 'numeric'],
            'taxes' => ['required', 'numeric'],
        ]);
        
        $product = new Product();
        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->taxes = $request->taxes;

        $product->save();

        return response()->json([
            'status' => 'ok',
            'product' => $product
        ]);
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
    }


    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'price' => ['required', 'numeric'],
            'taxes' => ['required', 'numeric'],
        ]);
        
        $product = Product::find($id);

        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->taxes = $request->taxes;

        $product->save();

        return response()->json([
            'status' => 'ok',
            'product' => $product
        ]);
    }


    public function destroy($id)
    {
        $product = Product::find($id);

        $product->delete();

        return response()->json([
            'status' => 'ok'
        ]);
    }

    public function getAllProduct() {
        $all_product = Product::all();

        return response()->json([
            'status' => 'ok',
            'all_product' => $all_product
        ]);
    }
}
