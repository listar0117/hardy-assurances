<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Countries;
use App\Customer;
use App\Product;
use App\Contract;
use PDF;
class HomeController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {

        $lastDateOfNextMonth =strtotime('last day of next month') ;
        $lastDay = date('Y-m-d', $lastDateOfNextMonth);
        $firstDateOfNextMonth =strtotime('first day of next month') ;
        $firstDay = date('Y-m-d', $firstDateOfNextMonth);

        $customers = Customer::whereDoesntHave('contracts', function (Builder $query) use ($firstDay, $lastDay) {
            $query->whereBetween('start_date', [$firstDay, $lastDay])
                    ->orWhereBetween('end_date', [$firstDay, $lastDay]);
        })->get();

        foreach ($customers as $customer) {
            $cur_list = $customer->contracts;
            foreach ($cur_list as $item) {
                $products = $item->products;
                foreach ($products as $product) {
                    $product['real_price'] = $product->pivot->real_price;
                }
                $item['products'] = $products;
                $item['customer'] = $item->customer;
            }
        }
        // die($customers);
        return view('dashboard', compact('customers'));
    }

    public function getCountries() {
        return Countries::getList('en', 'json');
    }

    public function showContracts($id) {
        $customer = Customer::find($id);
        $contract_list = $customer->contracts;
        foreach ($contract_list as $item) {
            $products = $item->products;
            foreach ($products as $product) {
                $product['real_price'] = $product->pivot->real_price;
            }
            $item['products'] = $products;
        }
        return view('contract.sub', compact('customer','contract_list'));
    }

    public function generatePDF($id) {

        $contract = Contract::find($id);
        $contract['customer'] = $contract->customer;
        $products = $contract->products;
        $sum_price = 0.0;
        $sum_taxes = 0.0;
        foreach ($products as $product) {
            $product['real_price'] = $product->pivot->real_price;
            $product['calc_taxes'] = round($product->pivot->real_price * $product->taxes / 100, 2);
            $sum_price += $product['real_price'];
            $sum_taxes += $product['calc_taxes'];
        }
        $contract['products'] = $products;
        $contract['sum_price'] = $sum_price;
        $contract['sum_taxes'] = $sum_taxes;
        $pdf = PDF::loadView('pdf', compact('contract'));
        return $pdf->stream();
    }    

}
