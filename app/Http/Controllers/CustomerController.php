<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Customer;

class CustomerController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $customer_list = Customer::all();
        return view('customer.index', compact('customer_list'));
    }

    public function create()
    {
        // return view('person.create');
    }


    public function store(Request $request)
    {
        
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'company' => ['nullable', 'string', 'max:255'],
            'email' => ['nullable', 'string', 'email', 'max:100', 'unique:customers'],
            'phone' => ['required', 'string', 'max:20', 'unique:customers'],
        ]);
        
        $customer = new Customer();
        $customer->name = $request->name;
        $customer->company = $request->company;
        $customer->email = $request->email;
        $customer->phone = $request->phone;
        $customer->address1 = $request->address1;
        $customer->address2 = $request->address2;
        $customer->address3 = $request->address3;
        $customer->country = $request->country;

        $customer->save();

        return response()->json([
            'status' => 'ok',
            'customer' => $customer
        ]);
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
    }


    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'company' => ['nullable', 'string', 'max:255'],
            'email' => ['nullable', 'string', 'email', 'max:100'],
            'phone' => ['required', 'string', 'max:20'],
        ]);
        
        $customer = Customer::find($id);
        $customer->name = $request->name;
        $customer->company = $request->company;
        $customer->email = $request->email;
        $customer->phone = $request->phone;
        $customer->address1 = $request->address1;
        $customer->address2 = $request->address2;
        $customer->address3 = $request->address3;
        $customer->country = $request->country;

        $customer->save();

        return response()->json([
            'status' => 'ok',
            'customer' => $customer
        ]);
    }


    public function destroy($id)
    {
        $customer = Customer::find($id);

        $customer->delete();

        return response()->json([
            'status' => 'ok'
        ]);
    }
}
