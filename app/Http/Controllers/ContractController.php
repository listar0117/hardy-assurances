<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contract;
class ContractController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $contract_list = Contract::all();

        foreach ($contract_list as $contract) {
            $contract['customer'] = $contract->customer;
            
            $products = $contract->products;
            foreach ($products as $product) {
                $product['real_price'] = $product->pivot->real_price;
            }
            $contract['products'] = $products;
        }
        return view('contract.index', compact('contract_list'));
    }

    public function create()
    {
        // return view('person.create');
    }


    public function store(Request $request)
    {
        
        $request->validate([
            'contract_number' => ['required', 'string', 'max:20'],
            'folder_name' => ['nullable', 'string', 'max:100'],
            'device_name' => ['nullable', 'string', 'max:100'],
            'start_date' => ['required', 'string', 'max:10'],
            'end_date' => ['required', 'string', 'max:10'],
        ]);
        
        $contract = new Contract();
        $contract->customer_id = $request->customer_id;
        $contract->contract_number = $request->contract_number;
        $contract->folder_name = $request->folder_name;
        $contract->device_name = $request->device_name;
        $contract->start_date = $request->start_date;
        $contract->end_date = $request->end_date;

        $contract->save();
        
        try {
            foreach ($request->products as $item) {
                $contract->products()->attach($item['id'], ['real_price' => $item['real_price'] ]);
            }
        }catch( \Exception $e){
            die ($e);
        }

        $products = $contract->products;
        foreach ($products as $product) {
            $product['real_price'] = $product->pivot->real_price;
        }
        $contract['products'] = $products;

        return response()->json([
            'status' => 'ok',
            'contract' => $contract
        ]);
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
    }


    public function update(Request $request, $id)
    {
        $request->validate([
            'contract_number' => ['required', 'string', 'max:20'],
            'folder_name' => ['nullable', 'string', 'max:100'],
            'device_name' => ['nullable', 'string', 'max:100'],
            'start_date' => ['required', 'string', 'max:10'],
            'end_date' => ['required', 'string', 'max:10'],
        ]);
        
        $contract = Contract::find($id);

        $contract->customer_id = $request->customer_id;
        $contract->contract_number = $request->contract_number;
        $contract->folder_name = $request->folder_name;
        $contract->device_name = $request->device_name;
        $contract->start_date = $request->start_date;
        $contract->end_date = $request->end_date;

        $contract->save();

        $contract->products()->detach();
        try {
            foreach ($request->products as $item) {
                $contract->products()->attach($item['id'], ['real_price' => $item['real_price'] ]);
            }
        }catch( \Exception $e){
            die ($e);
        }

        $products = $contract->products;
        foreach ($products as $product) {
            $product['real_price'] = $product->pivot->real_price;
        }
        $contract['products'] = $products;

        return response()->json([
            'status' => 'ok',
            'contract' => $contract
        ]);
    }


    public function destroy($id)
    {
        $contract = Contract::find($id);

        $contract->products()->detach();

        $contract->delete();

        return response()->json([
            'status' => 'ok'
        ]);
    }

    public function copyContract($id) {

        $contract = Contract::find($id);
        $new_contract = $contract->replicate();
        $new_contract->save();

        foreach ($contract->products as $product) {
            $new_contract->products()->attach($product->id, ['real_price' => $product->pivot->real_price ]);
        }

        $products = $new_contract->products;
        foreach ($products as $product) {
            $product['real_price'] = $product->pivot->real_price;
        }
        $new_contract['products'] = $products;

        return response()->json([
            'status' => 'ok',
            'contract' => $contract
        ]);
    }
}
