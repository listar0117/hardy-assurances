<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    //
    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }
    public function products() {
        return $this->belongsToMany('App\Product', 'prices')
                ->withPivot('real_price')
                ->withTimestamps();
    }
}
